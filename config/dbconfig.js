module.exports = {
    development: {
        username: 'root',
        password: 'root',
        database: 'hassicho.com',
        host: '127.0.0.1',
        port: '3306',
        dialect: 'mysql'
    },
    test: {
        username: 'root',
        password: 'root',
        database: 'hassicho.com',
        host: '127.0.0.1',
        port: '3306',
        dialect: 'mysql'
    },
    production: {
        use_env_variable: 'DATABASE_URL',
        dialect: 'mysql'
    }
};