module.exports = function(sequelize, DataTypes) {
    var user = sequelize.define('user', {
        amount: {
            type: DataTypes.INTEGER,
            allowNull: false
        },
        timestamp: DataTypes.DATE

    },{
        timestamps: false,
        underscored: true,
        freezeTableName: true
    });

    

    return user;
};