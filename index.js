var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var models = require('./apis/models/sequelize');
var jwt = require('express-jwt');
var jwks = require('jwks-rsa');
const jwtAuthz = require('express-jwt-authz');
const jwksRsa = require('jwks-rsa');
var index = require('./routes/index');
var users = require('./routes/users');
var env = process.env.NODE_ENV || 'development';
var app = express();

app.set('port', (process.env.PORT || 3000));


var jwtCheck = jwt({
  secret: jwks.expressJwtSecret({
      cache: true,
      rateLimit: true,
      jwksRequestsPerMinute: 5,
      jwksUri: "https://hassicho.auth0.com/.well-known/jwks.json"
  }),
  audience: 'http://localhost:3000/api/',
  issuer: "https://hassicho.auth0.com/",
  algorithms: ['RS256']
});

//app.use(jwtCheck);

app.get('/authorized', function (req, res) {
  res.send('Secured Resource');
});

const checkScopes = jwtAuthz(['read:messages']);

app.get('/api/public', function(req, res) {
  res.json({
    message: "Hello from a public endpoint! You don't need to be authenticated to see this."
  });
});

app.get('/api/private', jwtCheck, checkScopes, function(req, res) {
  res.json({
    message: 'Hello from a private endpoint! You need to be authenticated and have a scope of read:messages to see this.'
  });
});


// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

//routes
/*const checkScopes = jwtAuthz([ 'read:users' ]);

app.get('/api/private', checkJwt, checkScopes, function(req, res) {
  res.json({ 
    message: "Hello from a private endpoint! You need to be authenticated and have a scope of read:users to see this." 
  });
});*/
app.use('/api/users', function(req, res) {
  res.writeHead(301, "header");
  res.write(JSON.stringify({id:5,name:'hass'}, null, 2));
  res.end();
});
app.use('/', index);
app.use('/users', users);



models.sequelize.sync().then(function () {
  startApp();
});

function startApp() {
  console.log("Running enviornment: " + env);
  app.listen(app.get('port'), function () {
      console.log('Node app is running on port', app.get('port'));
  });
}


module.exports = app;
